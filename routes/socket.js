module.exports = function (socket) {
  var name = 'Guest_' + Math.floor((Math.random()*100)+1);

  socket.broadcast.emit('user:join', {
    name: name
  });

  socket.on('send:message', function (data) {
    socket.broadcast.emit('send:message', {
      user: data.user,
      text: data.message
    });
  });

}