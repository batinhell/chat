'use strict';

/* Controllers */

function ChatCtrl($scope, socket) {
  $scope.messages = [];
  // Listeners
  socket.on('user:join', function (data) {
    $scope.messages.push({
      user: 'admin',
      text: 'User ' + data.name + ' has joined'
    })
  });

  socket.on('send:message', function (message) {
    $scope.messages.push({
      user: message.user,
      text: message.text
      });
  });
  // On submit function
  $scope.sendMessage = function () {
    socket.emit('send:message', {
      message: $scope.message,
      user: $scope.name
    });
    // add the message to our model locally
    $scope.messages.push({
      user: $scope.name,
      text: $scope.message
    });

    // clear message box
    $scope.message = '';
  };

}

chat.controller('ChatCtrl', ['$scope', 'socket', ChatCtrl]);
