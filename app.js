var express = require('express')
  , app = express()
  , http = require('http')
  , routes = require('./routes')
  , socket = require('./routes/socket.js')


// Configuration
var config = require('./config')(app, express);


// Routes
app.get('/', routes.index);

// redirect all others to the index
app.get('*', routes.index);

// Start server
var port = process.env.PORT || 3000;
var io = require('socket.io').listen(app.listen(port));

// Socket.io Communication
io.sockets.on('connection', socket);